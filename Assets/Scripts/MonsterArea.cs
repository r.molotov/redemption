﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterArea : MonoBehaviour
{
    ForestMonsters fm;
    public GameObject MAW;

    bool returningUnit = false;

    public int ReturnUnitTime = 2;
    private void Start()
    {
        fm = transform.GetChild(0).GetComponent<ForestMonsters>();
    }
    void OnTriggerStay(Collider other)
    {
        if (returningUnit == false && other.GetComponent<CharController>() != null && fm.GetComponent<EnemyAI>().dmgDealerLast == other.gameObject)
        {
            fm.Attack(other.gameObject);
        }

    }
    void OnTriggerExit(Collider other)
    {
        if (returningUnit == false && other.GetComponent<CharController>() != null)
        {
            fm.ReturnHome(fm.spawnPoint.transform.position);           
        }
    }

    public IEnumerator ReturnUnit()
    {

        MAW.SetActive(false);
        fm.CanvasS.gameObject.SetActive(false);
        fm.ReturnHome(fm.spawnPoint.transform.position);
        CapsuleCollider cc = fm.GetComponent<CapsuleCollider>();
        cc.enabled = false;
        returningUnit = true;
        fm.agent.isStopped = false;

        yield return new WaitForSeconds(ReturnUnitTime);
        MAW.SetActive(true);
        fm.CanvasS.gameObject.SetActive(true);
        cc.enabled = true;
        returningUnit = false;
        EnemyAI ai = fm.GetComponent<EnemyAI>();
        ai.curHP = ai.maxHP;
        fm.GetComponent<EnemyAI>().dmgDealerLast = null;
        fm.agent.isStopped = false;

    }



}
