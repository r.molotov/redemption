﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    RaycastHit hit;
    void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = GameManager.Instance().cam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out hit, 10000)) return;

            EnemyAI e;
            if (e = hit.transform.GetComponent<EnemyAI>())
            {
                    GameManager.Instance().currentPlayer.Attack(e);
            }
            if (hit.transform.tag == "ground")
            {
                GameManager.Instance().currentPlayer.Move(hit.point, true);
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            GameManager.Instance().currentPlayer.CastingSpell(3);
            //print("J");
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            //GameManager.Instance().currentPlayer.CastingSpell(1);
        }

        if(Input.GetKeyDown(KeyCode.W))
            GameManager.Instance().currentPlayer.CastingSpell(2);

    }
}
