﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour
{
    CharController pc;
    public int speed;
    [HideInInspector] public float speedUp;

    //public GameObject targetIndicatorPrefab;
    //GameObject targetObject;
    // Start is called before the first frame update
    void Start()
    {
        pc = GetComponent<CharController>();
    }
    public void MoveA(Vector3 dest)
    {
        pc.State = 1;
        pc.agent.isStopped = false;
        pc.agent.speed = speed + speedUp;
        pc.agent.destination = dest;
        //targetObject.transform.position = pc.agent.destination;
        //targetObject.SetActive(true);
    }
}
