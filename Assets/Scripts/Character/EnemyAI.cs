﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemyAI : NetworkBehaviour
{
    public int ExpForKill;
    public float maxHP;
    [SyncVar] public float curHP;
    public Transform damgPoint;
    float DamageRecieveTime;
    public int StartRecoveringTime;

    public int HealPerSecond;

    public float maxMana;
    [HideInInspector] public float mana;
    [HideInInspector] public GameObject dmgDealerLast;

    [SyncVar] float hpNet;


    // Start is called before the first frame update
    void Start()
    {
            curHP = maxHP;
            mana = maxMana;
    }
    private void Update()
    {


        DamageRecieveTime += Time.deltaTime;
        if (curHP < maxHP && DamageRecieveTime > StartRecoveringTime)
        {
            curHP += HealPerSecond * Time.deltaTime;
        }
        if (mana < maxMana && DamageRecieveTime > StartRecoveringTime)
        {
            mana += HealPerSecond * Time.deltaTime;
        }
    }

    public void RecieveDamage(float dmg, GameObject damageDealer) {
        DamageRecieveTime = 0;
        curHP -= dmg;
        Die(damageDealer);
        dmgDealerLast = damageDealer;
    }

    public void CastingSpell()
    {
        DamageRecieveTime = 0;
    }

    void Die(GameObject killer)
    {
        if (curHP <= 0)
        {
            if (killer.GetComponent<CharController>() != null)
            {
                if (this.tag == "monster")
                {
                    ForestMonsters fm = this.GetComponent<ForestMonsters>();
                    fm.selectedUnit = null;
                    fm.enemyStats = null;
                    fm.GetComponentInParent<MonsterArea>().StartCoroutine("ReturnUnit");
                    print("KK");
                    killer.GetComponent<CharController>().selectedUnit = null;
                    killer.GetComponent<CharController>().State = 0;
                }
                killer.GetComponent<LevelSystem>().RecieveExp(ExpForKill);
            }
            else if(killer.GetComponent<ForestMonsters>() != null)
            {
                if (this.tag == "Player")
                {
                    CharController cc = this.GetComponent<CharController>();
                    cc.Die();
                    ForestMonsters fm = killer.GetComponent<ForestMonsters>();
                    fm.ReturnHome(fm.spawnPoint.transform.position);
                    
                }
            }
        }
    }

    [Client]
    void TransmitHP()
    {
        CmdSyncHP(curHP);
    }

    [Command]
    void CmdSyncHP(float hp)
    {
        hpNet = hp;
    }
}

