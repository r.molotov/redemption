﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameManager _instance;

    //[HideInInspector] 
    public CharController currentPlayer;
    //[HideInInspector] 
    public Camera cam;
    public static GameManager Instance() { return _instance; }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
