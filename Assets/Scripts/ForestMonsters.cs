﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
public class ForestMonsters : MonoBehaviour
{

    private int _state;
    public int State { 
    
        get { return _state; }
        set { _state = value; anim.SetInteger("state", _state); }
    }

    public Text hp;
    public Slider hpSld;
    public Canvas CanvasS;
    public Transform spawnPoint;
    public float speed;
    public float attackDmg;
    public float attackRange;

    EnemyAI ai;
    [HideInInspector] public NavMeshAgent agent;
    [HideInInspector] public bool enemyInRadius = false;
    [HideInInspector]public Animator anim;

    //Attack
    [HideInInspector] public EnemyAI enemyStats;
    public GameObject selectedUnit;

    private void Start()
    {
        ai = GetComponent<EnemyAI>();
        anim = GetComponent<Animator>();
        State = 0;
        agent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        hp.text = ai.curHP.ToString();
        hpSld.maxValue = ai.maxHP;
        hpSld.value = ai.curHP;
        CanvasS.transform.rotation = Quaternion.Euler(58, 0, 0);
        if (agent.hasPath && State <= 2 && agent.isStopped == false)
        {
            State = 1;
        }
            
        else if (!agent.hasPath && State <= 2)
            State = 0;
    }

    public void Attack(GameObject enemy)
    {
        selectedUnit = enemy;
        if (Vector3.Distance(transform.position, enemy.transform.position) < attackRange)
        {
            State = 2;
            transform.LookAt(selectedUnit.transform);
            agent.isStopped = true;
            enemyStats = enemy.GetComponent<EnemyAI>();
            //print(Vector3.Distance(transform.position, enemy.transform.position));
        }
        else
        {
            ForestMonsterMove(enemy.transform.position);
        }
    }



    public void ForestMonsterMove(Vector3 dest)
    {
        State = 1;
        agent.isStopped = false;
        agent.speed = speed;
        agent.destination = dest;
    }
    public void ReturnHome(Vector3 dest)
    {
        agent.isStopped = false;
        GetComponent<EnemyAI>().dmgDealerLast = null;
        agent.destination = dest;
        selectedUnit = null;
        enemyStats = null;
        State = 1;
    }


    void BasicAttack()
    {
        enemyStats.RecieveDamage(attackDmg, gameObject);
    }

}
