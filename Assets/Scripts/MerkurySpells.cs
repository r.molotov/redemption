﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MerkurySpells : MonoBehaviour
{
    [Header("1")]
    public float sp1CD;
    public GameObject BlackBall;
    public Transform BlackBallPoint;
    [HideInInspector] public Transform rot;
    public float manaCoast1;


    [Header("2")]
    public float sp2CD;
    public float dmgBaffCount;
    public Slider SP2UI;
    public float manaCoast2;
    bool spell2Cast= false;



    [Header("3")]
    public float healthUp;
    public float healthUpCD;
    public Slider SP3UI;
    public float manaCoast3;

    CharController pc;
    EnemyAI ai;

    private void Start()
    {
        pc = GetComponent<CharController>();
        ai = GetComponent<EnemyAI>();
    }

    public void SpellCall(int spellNumber)
    {
        //pc.agent.isStopped = true;
        switch (spellNumber)
        {
            
            case 1:
                if (pc.SP1 >= sp1CD && ai.mana >= manaCoast1)
                {
                    pc.State = 3;
                    pc.agent.isStopped = true;
                    ai.CastingSpell();
                }
                break;
            case 2:
                if (pc.SP2 >= sp2CD && ai.mana >= manaCoast2)
                {
                    Spell2();
                    ai.CastingSpell();
                }
                break;
            case 3:
                //Spell2();
                if (pc.SP3 >= healthUpCD && ai.mana >= manaCoast3)
                {
                    pc.State = 5;
                    pc.agent.isStopped = true;
                    ai.CastingSpell();
                }
                break;
        }


    }
    private void Update()
    {
        UI();
    }
    void Spell1()
    {
        print("K");
    }

    void Spell2()
    {
            pc.dmgBaff1 += dmgBaffCount;
            print("+");
            ai.mana -= manaCoast2;
            pc.SP2 = 0;
            ai.CastingSpell();
            spell2Cast = true;
        
    }


    void Spell3()
    {
        //print("kj");
        if (pc.SP3 >= healthUpCD)
        {
           
            EnemyAI health = GetComponent<EnemyAI>();
            if (health.curHP < health.maxHP - healthUp)
            {
                health.curHP += healthUp;
            }
            else if (health.curHP > health.maxHP - healthUp)
            {
                health.curHP = health.maxHP;
            }
            pc.SP3 = 0;
            //pc.State = 0;

            ai.mana -= manaCoast3;
        }
    }

    void State0()
    {
        pc.State = 0;
    }


    public void UpgradeStats(float UpdCoaf)
    {
        healthUp *= UpdCoaf;
        healthUpCD /= UpdCoaf;
        //print(healthUp +"__"+ healthUpCD);
    }

    void UI()
    {
        SP3UI.maxValue = healthUpCD;
        SP3UI.value = pc.SP3;

        SP2UI.maxValue = sp2CD;
        SP2UI.value = pc.SP2;
    }
    
}
