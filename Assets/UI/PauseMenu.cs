﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GamePause = false;
    public GameObject PauseMenuUI;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GamePause)
                Resume();
            else
                Pause();
        }
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        GamePause = false;
        Time.timeScale = 1;
    }

    void Pause()
    {
        PauseMenuUI.SetActive(true);
        GamePause = true;
        Time.timeScale = 0f;
    } 

    public void Quit()
    {
        print("OUT");
        Application.Quit();

    }

    
}
